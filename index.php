<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" type="text/css" href="style.css">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
    integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

  <title>Pengolahan Data</title>
</head>

<body>
  <!-- Image and text -->
  <div class="container  mb-3">
    <!-- Image and text -->
    <nav class="navbar navbar-light">
      <div class="container">
        <a class="navbar-brand">
          Teknik Informatika
        </a>
      </div>
    </nav>
  </div>
  <div class="jumbotron jumbotron-fluid">
    <div class="container mt-5">
      <h1 class="display-4">Data Mahasiswa</h1>
      <p class="lead">Isi data untuk memperoleh nilai akhir!</p>
    </div>
  </div>
  <div class="container mb-5 ">
    <div class="card">
      <div class="card-body">
        <form method="POST" action="hasil.php">
          <div class="form-group">
            <label for="nama">Nama:</label>
            <input type="text" class="form-control" name="nama" placeholder="Nama lengkap anda" required>
          </div>
          <div class="form-group">
            <label for="nim">NIM:</label>
            <input type="number" class="form-control" name="nim" placeholder="Nomer Induk Mahasiswa" required>
          </div>
          <div class="form-group">
            <label for="mapel">Mata pelajaran:</label> <br>
            <select name="pelajaran" class="form-control">
              <option value="Matematika">Matematika</option>
              <option value="Pemrograman web">Pemrograman web</option>
              <option value="Pemrograman mobile">Pemrograman mobile</option>
            </select>
          </div>
          <div class="form-group">
            <label>Nilai UTS:</label>
            <input type="number" min="0" max="100" class="form-control" name="uts" placeholder="0-100" required>
          </div>
          <div class="form-group">
            <label>Nilai Tugas:</label>
            <input type="number" min="0" max="100" class="form-control" name="tugas" placeholder="0-100" required>
          </div>
          <div class="form-group">
            <label>Nilai UAS:</label>
            <input type="number" min="0" max="100" class="form-control" name="uas" placeholder="0-100" required>
          </div>

          <button type="submit" class="btn btn-primary" id="button">Submit</button>
      </div>
    </div>
    </form>
  </div>
  <footer class="mt-5">
    <div class="container">
      <p class="text-center">&copy Dyah Kusuma</p>
    </div>
  </footer>


  <!-- Optional JavaScript; choose one of the two! -->

  <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
    crossorigin="anonymous"></script>

  <!-- Option 2: Separate Popper and Bootstrap JS -->
  <!--
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    -->
</body>

</html>