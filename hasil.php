<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <?php 
    $nama = @$_POST['nama'];
    $nim = @$_POST['nim'];
    $uts = @$_POST['uts'];
    $tugas = @$_POST['tugas'];
    $uas = @$_POST['uas'];
    $select = $_POST['pelajaran'];
    
    $nilai_uts = $uts*0.35;
    $nilai_uas = $uas*0.5;
    $nilai_tugas = $tugas*0.15;

    $nilai_akhir= $nilai_tugas+$nilai_uas+$nilai_uts;

    if($nilai_akhir>=90&& $nilai_akhir<=100){
        $grade ="A";
    }
    elseif ($nilai_akhir>70 && $nilai_akhir<90) {
        $grade ="B";
    }
    elseif ($nilai_akhir>50 && $nilai_akhir<=70) {
        $grade ="C";
    }
    elseif ($nilai_akhir<=50) {
        $grade ="D";
    }
    ?>

    <title>Pengolahan Data</title>
</head>

<body>
    <div class="container mb-3">
        <!-- Image and text -->
        <nav class="navbar navbar-light">
            <div class="container">
            <a class="navbar-brand">
                Teknik Informatika
            </a>
            </div>
        </nav>
    </div>
    <div class="jumbotron jumbotron-fluid">
        <div class="container mt-5">
            <h1 class="display-4">Data Mahasiswa</h1>
            <p class="lead">Hasil Nilai Akhir</p>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="card col-12 mb-3" style="width: 60rem;">
                    <div class="card-body">
                        <h5 class="card-title">Nama Mahasiswa:</h5>
                        <p class="card-text">
                            <?php 
                                echo "$nama";
                            ?>
                        </p>
                    </div>
                </div>
                <div class="card col-12 mb-3" style="width: 60rem;">
                    <div class="card-body">
                        <h5 class="card-title">Nomer Induk Mahasiswa:</h5>
                        <p class="card-text">
                            <?php 
                                echo "$nim";
                            ?>
                        </p>
                    </div>
                </div>
                <div class="card col-12 mb-3" style="width: 60rem;">
                    <div class="card-body">
                        <h5 class="card-title">Mata Pelajaran:</h5>
                        <p class="card-text">
                            <?php 
                            echo "$select";
                            ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card col-12 mb-3" style="width: 60rem;">
                    <div class="card-body">
                        <h5 class="card-title">Nilai UTS, UAS, TUGAS:</h5>
                        <p class="card-text">
                            <?php 
                                echo "Nilai UTS: $uts";
                            ?>
                        </p>
                        <p class="card-text">
                            <?php 
                                echo "Nilai UAS: $uas";
                            ?>
                        </p>
                        <p class="card-text">
                            <?php 
                                echo "Nilai tugas: $tugas";
                            ?>
                        </p>
                    </div>
                </div>
                <div class="card col-12 mb-3" style="width: 60rem;">
                    <div class="card-body">
                        <h5 class="card-title">HASIL:</h5>
                        <p class="card-text">
                            <?php 
                                echo "Total Nilai: $nilai_akhir";
                            ?>
                        </p>
                        <p class="card-text">
                            <?php 
                                echo "GRADE: $grade";
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>


    <footer class="mt-5">
        <div class="container">
            <p class="text-center">&copy Dyah Kusuma</p>
        </div>
    </footer>


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
        crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    -->
</body>

</html>